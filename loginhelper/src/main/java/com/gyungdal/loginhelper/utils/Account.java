package com.gyungdal.loginhelper.utils;

import com.gyungdal.loginhelper.helper.SignUpHelper;

/**
 * Created by 9274a on 2017-04-01.
 */

public class Account {
    public SignUpHelper data;
    private static Account instance;

    public static Account getInstance(){
        if(instance == null) {
            instance = new Account();
            instance.data = new SignUpHelper();
        }
        return instance;
    }
}
