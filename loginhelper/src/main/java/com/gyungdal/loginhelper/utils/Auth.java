package com.gyungdal.loginhelper.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.gyungdal.loginhelper.helper.SignUpHelper;
import com.gyungdal.loginhelper.helper.FirebaseHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 9274a on 2017-04-01.
 */

public class Auth {
    private SignUpHelper info;
    private Context mContext;

    public Auth(){
    }

    public Auth(Context mContext, String email, String pw){
        this.mContext = mContext;
        this.info = new SignUpHelper(email, pw, null, null);
    }

    public Auth(Context mContext, String email, String pw, String name, String call){
        this.mContext = mContext;
        this.info = new SignUpHelper(email, pw, name, call);
    }

    public Auth(Context mContext, SignUpHelper info){
        this.mContext = mContext;
        this.info = info;
    }

    public void register(){
        Map<String, Object> register = new HashMap<>();
        info.pw = BCrypt.hashpw(info.pw, BCrypt.gensalt(12));
        Log.i("EMAIL", info.email);
        Log.i("PW", info.pw);
        Log.i("NAME", info.name);
        Log.i("CALL", info.call);
        register.put(info.call, info);
        FirebaseHelper.getReferenceAccount().updateChildren(register, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Toast.makeText(mContext, "회원가입이 완료되었습니다. 로그인 후 사용가능합니다.", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mContext, "code:" + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void login(){
        FirebaseHelper.getReferenceAccount().addListenerForSingleValueEvent(new ValueEventListener() {
            final String email = info.email;
            final String pw = info.pw;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean hasAccount = false;

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    SignUpHelper loginModel = data.getValue(SignUpHelper.class);
                    if (loginModel.email.equals(info.email)) {
                        if (BCrypt.checkpw(pw, loginModel.pw)) {
                            hasAccount = true;
                            Account.getInstance().data.email = loginModel.email;
                            Account.getInstance().data.name = loginModel.name;
                            Account.getInstance().data.call = loginModel.call;
                            Account.getInstance().data.pw = loginModel.pw;
                        }
                        break;
                    }
                }

                if (!hasAccount) {
                    Toast.makeText(mContext, "계정이 존재하지 않습니다. 회원가입 후 로그인 바랍니다.", Toast.LENGTH_SHORT).show();
                }else{
                    Log.i("LOGIN!", Account.getInstance().data.email);
                    Toast.makeText(mContext, String.format("HELLO %s", Account.getInstance().data.email), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "오류가 발생했습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
