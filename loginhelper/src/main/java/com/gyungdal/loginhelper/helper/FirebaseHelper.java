package com.gyungdal.loginhelper.helper;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by 9274a on 2017-04-01.
 */

public class FirebaseHelper {
    public static FirebaseDatabase mDatabase;
    public static DatabaseReference mReferenceAccount;

    public static DatabaseReference getReferenceAccount(){
        if(mReferenceAccount == null){
            getDatabases();
            mReferenceAccount = mDatabase.getReference("account");
        }
        return mReferenceAccount;
    }
    public static FirebaseDatabase getDatabases(){
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }
}
