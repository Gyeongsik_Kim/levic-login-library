package com.gyungdal.test;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.newbie.levic.helper.accounthelper.utils.Auth;


public class MainActivity extends AppCompatActivity {
    private EditText email, pw, call, name;
    private Button photo, login, register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = (EditText)findViewById(R.id.email);
        call = (EditText)findViewById(R.id.call);
        pw = (EditText)findViewById(R.id.pw);
        name = (EditText)findViewById(R.id.name);
        email.setText("9274aa@gmail.com");
        call.setText("01089537414");
        name.setText("test");
        pw.setText("aa1003");
        login = (Button)findViewById(R.id.login);
        register = (Button)findViewById(R.id.register);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auth auth =
                        new Auth(getApplicationContext(),
                                email.getText().toString(), pw.getText().toString());
                auth.login();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);//one can be replaced with any action code

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Auth auth =
                            new Auth(getApplicationContext(),
                                    email.getText().toString(), pw.getText().toString(),
                                    name.getText().toString(), call.getText().toString());

                    auth.register(imageReturnedIntent.getData());
                }
                break;
        }
    }
}

