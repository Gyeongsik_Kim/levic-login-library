package com.newbie.levic.helper.accounthelper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.newbie.levic.helper.accounthelper.helper.FirebaseHelper;
import com.newbie.levic.helper.accounthelper.helper.PreferencesHelper;
import com.newbie.levic.helper.accounthelper.helper.SignUpHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 9274a on 2017-04-01.
 */

public class Auth {
    private SignUpHelper info;
    private Context mContext;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();

    private void signInAnonymously() {
        mAuth.signInAnonymously().addOnSuccessListener(new  OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                // do your stuff
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e("LOGIN", "signInAnonymously:FAILURE", exception);
            }
        });
    }

    public Auth(Context mContext, String email, String pw){
        this.mContext = mContext;
        this.info = new SignUpHelper(email, pw, null, null);
    }

    public Auth(Context mContext, String email, String pw, String name, String call){
        this.mContext = mContext;
        this.info = new SignUpHelper(email, pw, name, call);
    }

    public Auth(Context mContext, SignUpHelper info){
        this.mContext = mContext;
        this.info = info;
    }
    private byte[] loadPhoto(Uri photo){
        Bitmap bmp = null;
        try {
            bmp = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), photo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public void register(Uri photo){
        if (user == null)
            signInAnonymously();

        Log.i("photo", photo.getPath());
        String savePath = "profile_photo/"+info.email.replaceAll("\\W","_") + ".jpeg";
        Log.i("path", savePath);

        final Map<String, Object> register = new HashMap<>();
        info.pw = BCrypt.hashpw(info.pw, BCrypt.gensalt(12));
        Log.i("EMAIL", info.email);
        Log.i("PW", info.pw);
        Log.i("NAME", info.name);
        Log.i("CALL", info.call);
// Upload file and metadata to the path 'images/mountains.jpg'
        StorageReference profilePhoto = FirebaseHelper.getProfilePhotoStorage().child(savePath);
        UploadTask uploadTask = profilePhoto.putBytes(loadPhoto(photo));
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                info.profilePhotoPath = taskSnapshot.getDownloadUrl().toString();
                Toast.makeText(mContext, "프로파일 업로드 성공", Toast.LENGTH_SHORT).show();
                register.put(info.call, info);
                FirebaseHelper.getReferenceAccount().updateChildren(register, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                            Toast.makeText(mContext, "회원가입이 완료되었습니다. 로그인 후 사용가능합니다.", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(mContext, "code:" + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void login(){
        if (user == null)
            signInAnonymously();

        FirebaseHelper.getReferenceAccount().addListenerForSingleValueEvent(new ValueEventListener() {
            final String email = info.email;
            final String pw = info.pw;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean hasAccount = false;
                PreferencesHelper preference = new PreferencesHelper(mContext);
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    SignUpHelper loginModel = data.getValue(SignUpHelper.class);
                    if (loginModel.email.equals(info.email)) {
                        if (BCrypt.checkpw(pw, loginModel.pw)) {
                            hasAccount = true;
                            preference.savePreferences("profile_photo", loginModel.profilePhotoPath);
                            preference.savePreferences("email", loginModel.email);
                            preference.savePreferences("name", loginModel.name);
                            preference.savePreferences("is_google", "1");
                        }
                        break;
                    }
                }

                if (!hasAccount) {
                    Toast.makeText(mContext, "계정이 존재하지 않습니다. 회원가입 후 로그인 바랍니다.", Toast.LENGTH_SHORT).show();
                }else{
                    Log.i("LOGIN!",preference.getPreferences("email") );
                    Toast.makeText(mContext, String.format("HELLO %s", preference.getPreferences("email")), Toast.LENGTH_SHORT).show();
                    Toast.makeText(mContext, preference.getPreferences("profile_photo"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "오류가 발생했습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
