package com.newbie.levic.helper.accounthelper.helper;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


/**
 * Created by 9274a on 2017-04-01.
 */

public class FirebaseHelper {
    public static FirebaseDatabase mDatabase;
    public static DatabaseReference mReferenceAccount;
    public static StorageReference mProfilePhotoStorage;
    public static FirebaseStorage mStorage;

    static {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        if (mStorage == null) {
            mStorage = FirebaseStorage.getInstance();
        }
    }
    public static StorageReference getProfilePhotoStorage(){
        if(mProfilePhotoStorage == null) {
            mProfilePhotoStorage = mStorage.getReferenceFromUrl("gs://dummy-4b22c.appspot.com/");
        }
        return mProfilePhotoStorage;
    }
    public static DatabaseReference getReferenceAccount(){
        if(mReferenceAccount == null){
            mReferenceAccount = mDatabase.getReference("account");
        }
        return mReferenceAccount;
    }
}
