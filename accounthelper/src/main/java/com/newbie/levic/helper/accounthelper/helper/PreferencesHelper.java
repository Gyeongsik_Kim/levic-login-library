package com.newbie.levic.helper.accounthelper.helper;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by 9274a on 2017-04-01.
 */

public class PreferencesHelper {
    private Context mContext;
    public PreferencesHelper(Context mContext){
        this.mContext = mContext;
    }

    public String getPreferences(String key){
        SharedPreferences pref = mContext.getSharedPreferences("user", mContext.MODE_PRIVATE);
        return pref.getString(key, "");
    }

    public void savePreferences(String key, String value){
        SharedPreferences pref = mContext.getSharedPreferences("user", mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    // 값(Key Data) 삭제하기
    private void removePreferences(String key){
        SharedPreferences pref = mContext.getSharedPreferences("user", mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.commit();
    }

    // 값(ALL Data) 삭제하기
    private void removeAllPreferences(){
        SharedPreferences pref = mContext.getSharedPreferences("user", mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
