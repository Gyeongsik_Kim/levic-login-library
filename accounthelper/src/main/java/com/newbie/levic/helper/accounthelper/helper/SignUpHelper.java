package com.newbie.levic.helper.accounthelper.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 9274a on 2017-04-01.
 */

public class SignUpHelper implements Serializable {
    public String email;
    public String pw;
    public String name;
    public String call;
    public String profilePhotoPath;

    public SignUpHelper(){

    }

    public SignUpHelper(String email, String pw, String name, String call){
        this.email = email;
        this.pw = pw;
        this.name = name;
        this.call = call;
    }

    public Map<String, String> toMap(){
        Map<String, String> info = new HashMap<>();
        info.put("email", email);
        info.put("pw", pw);
        info.put("name", name);
        info.put("call", call);
        return info;
    }
}
